// Функція для обчислення факторіалу
const calculateFactorial = (n) => {
    if (n < 0) {
      return "Факторіал визначений лише для невід'ємних цілих чисел.";
    }
  
    if (n === 0 || n === 1) {
      return 1;
    }
  
    let factorial = 1;
    for (let i = 2; i <= n; i++) {
      factorial *= i;
    }
  
    return factorial;
  };
  
  // Зчитуємо число від користувача через модальне вікно
  const userInput = prompt('Введіть ціле число для обчислення факторіалу:');
  const number = parseInt(userInput);
  
  // Перевіряємо, чи користувач ввів число
  if (!isNaN(number)) {
    // Обчислюємо факторіал та виводимо результат на екран
    const result = calculateFactorial(number);
    console.log(`Факторіал числа ${number} дорівнює: ${result}`);
  } else {
    console.log('Введено некоректне число.');
  }
  